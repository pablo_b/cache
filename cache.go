package cache

import (
	"sync"
	"time"
)

type Cache[T any] struct {
	elements map[string]T
	rmw *sync.RWMutex
}

func New[T any](count int) *Cache[T]{
	if (count <= 0) {
		count = 50
	}

	return &Cache[T]{
		elements: make(map[string]T, count),
		rmw: new(sync.RWMutex),
	}
}

func (c *Cache[T]) Get(key string) (T, bool) {
	c.rmw.RLock()
	defer c.rmw.RUnlock()
	v, ok := c.elements[key]
	return v, ok
}

func (c *Cache[T]) Set(key string, val T) {
	c.rmw.Lock()
	c.elements[key] = val
	c.rmw.Unlock()
}


func (c *Cache[T]) SetTTL(key string, val T, ttl time.Duration) {
	c.Set(key, val)

	if (ttl > 0 ) {
		go func (c *Cache[T], ttl time.Duration) {
			<-time.After(ttl)
			c.Delete(key)
		}(c, ttl)
	}
}

func (c *Cache[T]) Delete(key string) {
	c.rmw.Lock()
	delete(c.elements, key)
	c.rmw.Unlock()
}